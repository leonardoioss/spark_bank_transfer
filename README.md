# Spark Bank Transfer

## This is an API for bank transactions (create clients, create accounts, get account balancae and amoutn transfer between accounts).

## MAIN CLASS:
	`BankTransfer.java`

## ENDPOINTS:

	ACCOUNT
	* [GET] ACCOUNT BY ID: http://localhost:4567/account/[:accountNumber]
		:accountNumber => Long

	* [POST] TRANSFER AMOUNT: http://localhost:4567/account/transfer

		{
			"sourceAccount" : Long,
			"destinationAccount" : Long,
			"amount" : Double
		}

	* [GET] LIST ALL ACCOUNTS: http://localhost:4567/account/list

	* [GET] GET BALANCE: http://localhost:4567/account/:accountNumber/getBalance
		:accountNumber => Long

	* [POST] SAVE NEW ACCOUNT: http://localhost:4567/account/save

		{
			"bankBranch" : Long,
			"accountNumber" : Long,
			"clientDocument" : Long,
			"balance" : Double
		}

	CLIENT
	* [POST] SAVE NEW CLIENT: http://localhost:4567/client/save

		{
		    "fullName": String,
		    "document": Long,
		    "birthdate": String,
		    "address": {
		    	"street" : String,
		    	"number" : Long,
		    	"complement" : String,
		    	"city" : String,
		    	"country" : String,
		    	"zipNumber" : String
		    }
		}

