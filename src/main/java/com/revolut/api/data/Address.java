/**
 * 
 */
package com.revolut.api.data;

/**
 * Class that represents a client's address
 * @author Leonardo
 *
 */
public class Address {

	private String street;
	
	private Integer number;
	
	private String complement;

	private String city;
	
	private String country;
	
	private String zipNumber;

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the number
	 */
	public Integer getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(Integer number) {
		this.number = number;
	}

	/**
	 * @return the complement
	 */
	public String getComplement() {
		return complement;
	}

	/**
	 * @param complement the complement to set
	 */
	public void setComplement(String complement) {
		this.complement = complement;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the zipNumber
	 */
	public String getZipNumber() {
		return zipNumber;
	}

	/**
	 * @param zipNumber the zipNumber to set
	 */
	public void setZipNumber(String zipNumber) {
		this.zipNumber = zipNumber;
	}
	
}
