/**
 * 
 */
package com.revolut.api.data;

import com.revolut.api.exception.BusinessException;

/**
 * Class that represents an account from a bank
 * @author Leonardo
 *
 */
public class Account {
	
	private Long bankBranch;
	
	private Long accountNumber;
	
	private Long clientDocument;
	
	private Double balance;

	/**
	 * @return the bankBranch
	 */
	public Long getBankBranch() {
		return bankBranch;
	}

	/**
	 * @param bankBranch the bankBranch to set
	 */
	public void setBankBranch(Long bankBranch) {
		this.bankBranch = bankBranch;
	}

	/**
	 * @return the accountNumber
	 */
	public Long getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the clientDocument
	 */
	public Long getAccountClientDocument() {
		return clientDocument;
	}

	/**
	 * @param clientDocument the clientDocument to set
	 */
	public void setAccountClientDocument(Long clientDocument) {
		this.clientDocument = clientDocument;
	}

	/**
	 * @return the balance
	 */
	public Double getBalance() {
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	
	public void transferValue(Account destinationAccount, double amount) throws BusinessException {
		if(this.getBalance() >= amount) {
			this.setBalance(this.getBalance() - amount);
			destinationAccount.setBalance(destinationAccount.getBalance() + amount);
		} else {
			throw new BusinessException("There is no enough balance to transfer.");
		}
	}
}
