package com.revolut.api.data;

/**
 * CLass that represents a client from a bank
 * @author Leonardo
 *
 */
public class Client {
	
	private String fullName;
	
	private Long document;
	
	private String birthdate;
	
	private Address address;

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the document
	 */
	public Long getDocument() {
		return document;
	}

	/**
	 * @param document the document to set
	 */
	public void setDocument(Long document) {
		this.document = document;
	}

	/**
	 * @return the birthdate
	 */
	public String getBirthdate() {
		return birthdate;
	}

	/**
	 * @param birthdate the birthdate to set
	 */
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}
	
}
