/**
 * 
 */
package com.revolut.api.client.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.revolut.api.data.Client;

/**
 * @author Leonardo
 *
 */
public class ClientDaoImpl implements IntClientDao {

	private static final Map<Long, Client> CLIENT_DB = new ConcurrentHashMap<Long, Client>();

	@Override
	public void createClient(Client client) {
		CLIENT_DB.put(client.getDocument(), client);
	}
	
	@Override
	public List<Client> listAll() {
		List<Client> clients = new ArrayList<Client>();
		CLIENT_DB.values().stream().forEach(client -> clients.add(client));
		return clients;
	}

	@Override
	public Client getClientByDocument(Long document) {
		return CLIENT_DB.get(document);
	}

	@Override
	public Long deleteClient(Long document) {
		CLIENT_DB.remove(document);
		return document;
	}

	@Override
	public Client updateClient(Client client) {
		CLIENT_DB.put(client.getDocument(), client);
		return client;
	}

	public boolean checkIfExists(Long document) {
		return CLIENT_DB.containsKey(document);
	}
	
}
