/**
 * 
 */
package com.revolut.api.client.dao;

import java.util.List;

import com.revolut.api.data.Client;

/**
 * @author Leonardo
 *
 */
public interface IntClientDao {

	public void createClient(Client client);
	
	public List<Client> listAll();
	
	public Client getClientByDocument(Long document);
	
	public Long deleteClient(Long document);
	
	public Client updateClient(Client client);
	
}
