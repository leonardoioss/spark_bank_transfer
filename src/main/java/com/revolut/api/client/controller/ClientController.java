/**
 * 
 */
package com.revolut.api.client.controller;

import static spark.Spark.post;

import org.eclipse.jetty.http.HttpStatus;

import com.google.gson.Gson;
import com.revolut.api.client.service.ClientService;
import com.revolut.api.data.Client;
import com.revolut.api.exception.BusinessException;
import com.revolut.api.util.EnumRESTMessage;
import com.revolut.api.util.JsonObjetc;

/**
 * @author Leonardo
 *
 */
public class ClientController {

	private ClientService clientService = new ClientService();
	
	public ClientController() {
		post("/client/save", (request, response) -> {
			response.type("application/json");
			Gson jsonResponse = new Gson();
			try {
				Client client = new Gson().fromJson(request.body(), Client.class);
				this.clientService.createClient(client);
				jsonResponse.toJson(new JsonObjetc(HttpStatus.CREATED_201, EnumRESTMessage.CREATE_CLIENT.getMessage()));				
			} catch (BusinessException e) {
				jsonResponse.toJson(new JsonObjetc(HttpStatus.UNAUTHORIZED_401, e.getMessage()));
			}
			return jsonResponse;
		});
		
	}
	
}
