/**
 * 
 */
package com.revolut.api.client.service;

import com.revolut.api.client.dao.ClientDaoImpl;
import com.revolut.api.data.Client;
import com.revolut.api.exception.BusinessException;

/**
 * @author Leonardo
 *
 */
public class ClientService {

	private ClientDaoImpl clientDaoImpl;
	
	public ClientService() {
		this.clientDaoImpl = new ClientDaoImpl();
	}
	
	public void createClient(Client client) throws BusinessException {
		
		if(this.clientDaoImpl.checkIfExists(client.getDocument())) {
			throw new BusinessException("There is already a client stored with this client document number.");
		}
		
		this.clientDaoImpl.createClient(client);
	}
	
}
