/**
 * 
 */
package com.revolut.api.account.service;

import java.util.List;

import com.revolut.api.account.dao.AccountDaoImpl;
import com.revolut.api.client.dao.ClientDaoImpl;
import com.revolut.api.data.Account;
import com.revolut.api.exception.BusinessException;
import com.revolut.api.util.TransferDTO;

/**
 * @author Leonardo
 *
 */
public class AccountService {

	private AccountDaoImpl accountDao;
	private ClientDaoImpl clientDaoImpl;
	
	public AccountService() {
		this.accountDao = new AccountDaoImpl();
		this.clientDaoImpl = new ClientDaoImpl();
	}
	
	public void createAccount(Account account) throws BusinessException {
		this.validateAccount(account);
		
		if(this.accountDao.checkIfExists(account.getAccountNumber())) {
			throw new BusinessException("There is already an account stored with this account number.");
		}
		
		this.accountDao.createAccount(account);
	}
	
	public List<Account> listAllAccounts() {
		return this.accountDao.listAll();
	}
	
	public Account getAccountByNumber(Long accountNumber) throws BusinessException {
		if(accountNumber == null) {
			throw new BusinessException("Invalid account number.");
		}
		
		if(!this.accountDao.checkIfExists(accountNumber)) {
			throw new BusinessException("There is no account with this account number.");
		}
		
		return this.accountDao.getAccountByNumber(accountNumber);
	}
	
	public void transferBetweenAccount(TransferDTO dto) throws BusinessException {
		if(dto.getSourceAccount() == null) {
			throw new BusinessException("Invalid source account number.");
		}
		
		if(dto.getDestinationAccount() == null) {
			throw new BusinessException("Invalid destination account number.");
		}
		
		if(!this.accountDao.checkIfExists(dto.getSourceAccount())) {
			throw new BusinessException("There is no source account with this number.");
		}
		
		if(!this.accountDao.checkIfExists(dto.getDestinationAccount())) {
			throw new BusinessException("There is no destination account with this number.");
		}
		
		if(dto.getDestinationAccount() == dto.getSourceAccount()) {
			throw new BusinessException("THe source and destination accounts are the same.");
		}
		
		Account sourceAccount = this.accountDao.getAccountByNumber(dto.getSourceAccount());
		Account destinationAccount = this.accountDao.getAccountByNumber(dto.getDestinationAccount());
		sourceAccount.transferValue(destinationAccount, dto.getAmount());
		
		this.accountDao.updateAccount(sourceAccount);
		this.accountDao.updateAccount(destinationAccount);
	}
	
	private void validateAccount(Account account) throws BusinessException {
		if(account == null) {
			throw new BusinessException("Invalid account.");
		}
		else if(account.getAccountClientDocument() == null) {
			throw new BusinessException("There is no client to this account.");
		}
		else if(account.getBankBranch() == null) {
			throw new BusinessException("Invalid account's bank branch.");
		}
		else if(account.getAccountNumber() == null) {
			throw new BusinessException("Invalid account number.");
		}
		
		if(!this.clientDaoImpl.checkIfExists(account.getAccountClientDocument())) {
			throw new BusinessException("There is no client stored with this document number");
		}
	}
}
