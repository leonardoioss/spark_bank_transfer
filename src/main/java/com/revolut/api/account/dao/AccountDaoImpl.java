/**
 * 
 */
package com.revolut.api.account.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.revolut.api.data.Account;

/**
 * @author Leonardo
 *
 */
public class AccountDaoImpl implements IntAccountDao {

	private Map<Long, Account> ACCOUNT_DB = new ConcurrentHashMap<Long, Account>();
	
	@Override
	public void createAccount(Account account) {
		ACCOUNT_DB.put(account.getAccountNumber(), account);
	}

	@Override
	public List<Account> listAll() {
		List<Account> accounts = new ArrayList<Account>();
		ACCOUNT_DB.values().stream().forEach(account -> accounts.add(account));
		return accounts;
	}

	@Override
	public Account getAccountByNumber(Long accountNumber) {
		return ACCOUNT_DB.get(accountNumber);
	}

	@Override
	public Long deleteAccount(Long accountNumber) {
		ACCOUNT_DB.remove(accountNumber);
		return accountNumber;
	}

	@Override
	public void updateAccount(Account account) {
		ACCOUNT_DB.put(account.getAccountNumber(), account);
	}

	public boolean checkIfExists(Long accountNumber) {
		return ACCOUNT_DB.containsKey(accountNumber);
	}
}
