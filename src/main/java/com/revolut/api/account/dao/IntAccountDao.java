/**
 * 
 */
package com.revolut.api.account.dao;

import java.util.List;

import com.revolut.api.data.Account;

/**
 * @author Leonardo
 *
 */
public interface IntAccountDao {

	public void createAccount(Account account);

	public List<Account> listAll();

	public Account getAccountByNumber(Long accountNumber);

	public Long deleteAccount(Long accountNumber);

	public void updateAccount(Account account);

}
