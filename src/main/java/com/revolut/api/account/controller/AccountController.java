/**
 * 
 */
package com.revolut.api.account.controller;

import static spark.Spark.get;
import static spark.Spark.post;

import org.eclipse.jetty.http.HttpStatus;

import com.google.gson.Gson;
import com.revolut.api.account.service.AccountService;
import com.revolut.api.data.Account;
import com.revolut.api.exception.BusinessException;
import com.revolut.api.util.EnumRESTMessage;
import com.revolut.api.util.JsonObjetc;
import com.revolut.api.util.TransferDTO;

/**
 * @author Leonardo
 *
 */
public class AccountController {

	private AccountService accountService = new AccountService();
	
	public AccountController() {
		post("/account/save", (request, response) -> {
			response.type("application/json");
			Account account = new Gson().fromJson(request.body(), Account.class);
			Gson jsonResponse = new Gson();
			try {
				this.accountService.createAccount(account);
				jsonResponse.toJson(new JsonObjetc(HttpStatus.CREATED_201, EnumRESTMessage.CREATE_ACCOUNT.getMessage()));
			} catch (BusinessException e) {
				jsonResponse.toJson(new JsonObjetc(HttpStatus.UNAUTHORIZED_401, e.getMessage()));
			}
			return jsonResponse;
		});
		
		get("/account/list", (request, response) -> {
			response.type("application/json");
			Gson jsonResponse = new Gson();
			try {
				jsonResponse.toJson(new JsonObjetc(HttpStatus.OK_200, new Gson().toJsonTree(this.accountService.listAllAccounts())));
			} catch (Exception e) {
				jsonResponse.toJson(new JsonObjetc(HttpStatus.UNAUTHORIZED_401, e.getMessage()));
			}
			return jsonResponse;
		});
		
		get("/account/:accountNumber", (request, response) -> {
			response.type("application/json");
			Gson jsonResponse = new Gson();
			try {
				Account account = this.accountService.getAccountByNumber(Long.parseLong(request.params(":accountNumber")));
				jsonResponse.toJson(new JsonObjetc(HttpStatus.OK_200, new Gson().toJsonTree(account)));				
			} catch (BusinessException e) {
				jsonResponse.toJson(new JsonObjetc(HttpStatus.UNAUTHORIZED_401, e.getMessage()));
			}
			return jsonResponse;
		});
		
		post("/account/transfer", (request, response) -> {
			response.type("application/json");
			Gson jsonResponse = new Gson();
			try {
				TransferDTO dto = new Gson().fromJson(request.body(), TransferDTO.class);
				this.accountService.transferBetweenAccount(dto);
				jsonResponse.toJson(new JsonObjetc(HttpStatus.CREATED_201, EnumRESTMessage.TRANSFER_AMOUNT.getMessage()));
			} catch (BusinessException e) {
				jsonResponse.toJson(new JsonObjetc(HttpStatus.UNAUTHORIZED_401, e.getMessage()));
			}
			return jsonResponse;
		});
	}
	
}
