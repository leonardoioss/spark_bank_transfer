/**
 * 
 */
package com.revolut.api.util;

/**
 * @author Leonardo
 *
 */
public enum EnumRESTMessage {

	CREATE_CLIENT("Success creating client!"),
	CREATE_ACCOUNT("Success creating account!"),
	TRANSFER_AMOUNT("Success transfering amount!");
	
	String message;
	
	private EnumRESTMessage(String message) {
		this.setMessage(message);
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}
