/**
 * 
 */
package com.revolut.api.util;

/**
 * @author Leonardo
 *
 */
public class TransferDTO {

	private Long sourceAccount;
	
	private Long destinationAccount;
	
	private Double amount;
	
	public TransferDTO(Long srcNumber, Long destNumber, Double amount) {
		this.setSourceAccount(srcNumber);
		this.setDestinationAccount(destNumber);
		this.setAmount(amount);
	}

	/**
	 * @return the sourceAccount
	 */
	public Long getSourceAccount() {
		return sourceAccount;
	}

	/**
	 * @param sourceAccount the sourceAccount to set
	 */
	public void setSourceAccount(Long sourceAccount) {
		this.sourceAccount = sourceAccount;
	}

	/**
	 * @return the destinationAccount
	 */
	public Long getDestinationAccount() {
		return destinationAccount;
	}

	/**
	 * @param destinationAccount the destinationAccount to set
	 */
	public void setDestinationAccount(Long destinationAccount) {
		this.destinationAccount = destinationAccount;
	}

	/**
	 * @return the amount
	 */
	public Double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
}
