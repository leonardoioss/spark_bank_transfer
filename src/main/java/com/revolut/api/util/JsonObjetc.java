/**
 * 
 */
package com.revolut.api.util;

import com.google.gson.JsonElement;

/**
 * @author Leonardo
 *
 */
public class JsonObjetc {

	private Integer status;
	private String message;
	private JsonElement jsonData;
	
	public JsonObjetc(Integer status) {
		this.status = status;
	}
	
	public JsonObjetc(Integer status, JsonElement data) {
		this.status = status;
		this.jsonData = data;
	}
	
	public JsonObjetc(Integer status, String message) {
		this.status = status;
		this.message = message;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the jsonData
	 */
	public JsonElement getJsonData() {
		return jsonData;
	}

	/**
	 * @param jsonData the jsonData to set
	 */
	public void setJsonData(JsonElement jsonData) {
		this.jsonData = jsonData;
	}
}
